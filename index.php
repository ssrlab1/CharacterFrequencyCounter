<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'CharacterFrequencyCounter.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = CharacterFrequencyCounter::loadLanguages();
	CharacterFrequencyCounter::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo CharacterFrequencyCounter::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/sorttable.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', CharacterFrequencyCounter::INPUT_TEXT_DEFAULT)); ?>";
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#additionalResultId').empty();
					$('#additionalResultBlockId').hide('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					$('#resultBlockId').show('slow');
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/CharacterFrequencyCounter/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'certainCharacters': $('input#certainCharactersId').val()
						},
						success: function(msg){
							var result = jQuery.parseJSON(msg);
							$('#resultId').html(result.result);
							newTableObject = document.getElementById('resultTableId');
							sorttable.makeSortable(newTableObject);
						},
						error: function(){
							$('#resultId').html('ERROR');
						}
					});
				});
				$(document).on('click', '.char', function() {
					$('#additionalResultId').empty();
					$('#additionalResultId').prepend($('<img>', { src: "img/loading.gif"}));
					$('#additionalResultBlockId').show('slow');
					$.ajax({
						type: "POST",
						url: "https://corpus.by/CharacterFrequencyCounter/api.php",
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'certainCharacters': $(this).text()
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							$('#additionalResultId').html(result.result);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/CharacterFrequencyCounter/?lang=<?php echo $lang; ?>"><?php echo CharacterFrequencyCounter::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo CharacterFrequencyCounter::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo CharacterFrequencyCounter::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = CharacterFrequencyCounter::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . CharacterFrequencyCounter::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo CharacterFrequencyCounter::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo CharacterFrequencyCounter::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo CharacterFrequencyCounter::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", CharacterFrequencyCounter::INPUT_TEXT_DEFAULT); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<label for="certainCharactersId" class="col-sm col-form-label"><?php echo CharacterFrequencyCounter::showMessage('certain characters'); ?></label>
					<input type="text" id="certainCharactersId" name="certainCharacters" value="" class="input-primary">
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo CharacterFrequencyCounter::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="additionalResultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo CharacterFrequencyCounter::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="additionalResultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<p id="statisticsId"></p>
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo CharacterFrequencyCounter::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo CharacterFrequencyCounter::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/CharacterFrequencyCounter" target="_blank"><?php echo CharacterFrequencyCounter::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/CharacterFrequencyCounter/-/issues/new" target="_blank"><?php echo CharacterFrequencyCounter::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo CharacterFrequencyCounter::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo CharacterFrequencyCounter::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo CharacterFrequencyCounter::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php CharacterFrequencyCounter::sendErrorList($lang); ?>