<?php
	class CharacterFrequencyCounter {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $language = '';
		private $text = '';
		private $certainCharacters = array();
		private $allCharactersCnt = 0;
		private $resultTable = '';
		private $charactersToNames = array();
		const BR = "<br>\n";
		const INPUT_TEXT_DEFAULT = "Сірыус (лац. Sirius), таксама Альфа Вялікага Сабакі (лац. Alpha Canis Majoris) – найярчэйшая зорка начного неба. Бачная зорная велічыня -1,46. Сірыус аддалены ад Сонца на 8,60±0,04 св. гадоў і з'яўляецца адной з бліжэйшых да нас зорак. Нябесныя каардынаты: прамое ўзыходжанне 6 гадз. 45 хв. 9 сек., схіленне -16°42'58''. Назва паходзіць з грэцкай мовы і азначае \"бліскучы\" альбо \"пякучы\".\n\nСкарына (3283 Skorina, 1979 QA10) – астэроід № 3283. Належыць да Галоўнага пояса астэроідаў Сонечнай сістэмы. Дыяметр – 12,65 км. Сярэдняя аддаленасць астэроіда ад Сонца складае 2,396 астранамічнай адзінкі (~ 360 млн. км.). Перыяд поўнага абарачэння вакол Сонца – 3,71 зямнога года. Адкрыты астраномам Мікалаем Іванавічам Чарных у Крымскай астрафізічнай абсерваторыі 27 жніўня 1979 г. Названы ў гонар беларускага першадрукара Францыска Скарыны.";
		
		function __construct($language = 'be', $certainCharacters = '') {
			$this->language = $language;
			if(!empty($certainCharacters)) {
				$this->certainCharacters = preg_split('//u', $certainCharacters, -1, PREG_SPLIT_NO_EMPTY);
			}
			$this->readTable();
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Character Frequency Counter';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/CharacterFrequencyCounter/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		private function readTable() {
			$filePath = dirname(dirname(__FILE__)) . "/resources/CharacterInformationWindows1251.txt";
			$handle = fopen($filePath, 'r') OR die("fail open 'CharacterInformationWindows1251.txt'");
			if($handle) {
				while(($buffer = fgets($handle, 4096)) !== false) {
					if(substr($buffer, 0, 1) != "#") {
						$symbol_str = preg_split("/\t/", $buffer);
						$this->charactersToNames['be'][$symbol_str[2]] = $symbol_str[5];
						$this->charactersToNames['ru'][$symbol_str[2]] = $symbol_str[6];
						$this->charactersToNames['en'][$symbol_str[2]] = $symbol_str[4];
					}
				}
			}
			fclose($handle);
		}
		
		public function setText($text) {
			$this->text = $text;
		}
		
		public function run() {
			$charactersArr = $this->getAllCharacters($this->text);
			$this->resultTable = $this->formTable($charactersArr);
		}
		
		private function getAllCharacters($text) {
			$allCharactersArr = array();
			$lines_arr = $this->mb_str_split($this->text, 28);
			foreach($lines_arr as $line) {
				$chars = preg_split('//u', $line, -1, PREG_SPLIT_NO_EMPTY);
				foreach($chars as $char) {
					$char_code = $this->ordutf8($char);
					// search only for certain characters
					if(!empty($this->certainCharacters)) {
						if(array_search($char, $this->certainCharacters) !== false) {
							$allCharactersArr[$char_code]['char'] = $char;
							$allCharactersArr[$char_code]['line'][] = $line;
							if(isset($allCharactersArr[$char_code]['count'])) {
								$allCharactersArr[$char_code]['count']++;
							}
							else {
								$allCharactersArr[$char_code]['count'] = 1;
							}
						}
					}
					// search for all characters
					else {
						if(!isset($allCharactersArr[$char_code])) {
							$allCharactersArr[$char_code]['char'] = $char;
							$allCharactersArr[$char_code]['line'][] = $line;
							$allCharactersArr[$char_code]['count'] = 1;
						}
						else {
							$allCharactersArr[$char_code]['count']++;
						}
					}
					
					$this->allCharactersCnt++;
				}
			}
			return $allCharactersArr;
		}
		
		private function mb_str_split($string, $string_length = 1, $charset = 'utf-8') {
			if(mb_strlen($string, $charset) > $string_length || !$string_length) {
				do {
					$c = mb_strlen($string,$charset);
					$parts[] = mb_substr($string, 0, $string_length,$charset);
					$string = mb_substr($string, $string_length, $c-$string_length, $charset);
				} while(!empty($string));
			}
			else {
				$parts = array($string);
			}
			return $parts;
		}
		
		public function ordutf8($string) {
			$offset = 0;
			$code = ord(substr($string, $offset, 1));
			if($code >= 128) {														//otherwise 0xxxxxxx
				if($code < 224) $bytesnumber = 2;							//110xxxxx
				elseif($code < 240) $bytesnumber = 3;					//1110xxxx
				elseif($code < 248) $bytesnumber = 4;					//11110xxx
				$codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
				for ($i = 2; $i <= $bytesnumber; $i++) {
					$offset++;
					$code2 = ord(substr($string, $offset, 1)) - 128;	//10xxxxxx
					$codetemp = $codetemp*64 + $code2;
				}
				$code = $codetemp;
			}
			$offset += 1;
			if($offset >= strlen($string)) $offset = -1;
			$codehex = strtoupper(dechex($code));
			if(strlen($codehex) == 1) return "U+000$codehex";
			elseif(strlen($codehex) == 2) return "U+00$codehex";
			elseif(strlen($codehex) == 3) return "U+0$codehex";
			elseif(strlen($codehex) == 4) return "U+$codehex";
			else return $codehex;
		}
		
		private function formTable($charactersArr) {
			$result = '';
			if(!empty($charactersArr)) {
				$table = '<table id="resultTableId" class="table table-sm table-striped sortable" align="center" width=100%>';
				$table .= '<thead><tr><th scope="col" class="sorttable_alpha">';
				$table .= self::$localizationArr['character'];
				$table .= '</th><th scope="col" class="sorttable_alpha">';
				$table .= self::$localizationArr['code'];
				$table .= '</th><th scope="col" class="sorttable_alpha">';
				$table .= self::$localizationArr['character name'];
				$table .= '</th><th scope="col" class="sorttable_numeric">';
				$table .= self::$localizationArr['frequency'];
				$table .= '</th><th scope="col" class="sorttable_alpha">';
				$table .= self::$localizationArr['context'];
				$table .= '</th></tr></thead><tbody>';
				
				foreach($charactersArr as $code => $val) {
					if(isset($this->charactersToNames[$this->language][$code])) {
						$char_name = $this->charactersToNames[$this->language][$code];
					}
					else {
						$char_name = '–';
					}
					$contexts = implode("<br />", $val['line']);
					$contexts = str_replace($val['char'], '<font color="red">' . $val['char'] . '</font>', $contexts);
					$table .=	'<tr valign="top">';
					$table .=	'<td width=36 align="center"><p id="' . $code . '" class="char" style="color: red;"">' . $val['char'] . '</p></td>';
					$table .=	'<td width=76 align="center"><a href="https://unicode-table.com/en/' . mb_substr($code, 2) . '/">' . $code . '</a></td>';
					$table .=	'<td width=300>' . $char_name . '</td>';
					$table .=	'<td align="center">' . $val['count'] . ' (' . round($val['count']*100/$this->allCharactersCnt, 2) . '%)</td>';
					$table .=	'<td>' . $contexts . '</td>';
					$table .=	'</tr>';
				}
				$table .=  '</tbody></table>';
				
				$result .= self::$localizationArr['characters amount'] . ': <b>' . $this->allCharactersCnt . '</b>' . self::BR;
				$result .= self::$localizationArr['unique characters amount'] . ': <b>' . count($charactersArr) . '</b>' . self::BR . self::BR;
				$result .= $table;
			}
			return $result;
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$date_code = date('Y-m-d_H-i-s', time());
			$rand_code = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'CharacterFrequencyCounter';
			$sendersName = 'Character Frequency Counter';
			$senders_email = "corpus.by@gmail.com";
			$recipient = "corpus.by@gmail.com";
			$subject = "Character Frequency Counter from IP $ip";
			$mail_body = "Вітаю, гэта corpus.by!" . self::BR; 
			$mail_body .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$text_length = mb_strlen($this->text);
			$pages = round($text_length/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$cache_text = preg_replace("/(^\s+)|(\s+$)/us", "", $this->text);
			fwrite($new_file, $cache_text);
			fclose($new_file);
			$mail_body .= "Тэкст ($text_length сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
			$pos = mb_strpos($cache_text, "\n");
			if(empty($pos)) $pos = 1000;
			$mail_body .= "<i>\"" . mb_substr($cache_text, 0, min(150, $pos-1)) . "\"</i>" . self::BR;
			$mail_body .= "Зыходны тэкст цалкам захаваны тут: " . dirname(dirname(__FILE__)) . "/showCache.php?s=CharacterFrequencyCounter&t=in&f=$filename" . self::BR . self::BR;
			
			$filename = $date_code . '_'. $ip . '_' . $rand_code . '_out.html';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$new_file = fopen($filepath, 'wb') OR die('open cache file error');
			$html = '<html><head>';
			$html .= '<title>ІНФАРМАЦЫЯ ПРА ЎСЕ ЗНОЙДЗЕНЫЯ СІМВАЛЫ</title>';
			$html .= '<meta charset="utf-8" />';
			$html .= '<link rel="stylesheet" type="text/css" media="all" href="../_css/style.css"/>';
			$html .= "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,300,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css' />";
			$html .= '</head>';
			$html .= '<body>';
			$html .= '<b>ІНФАРМАЦЫЯ ПРА ЎСЕ ЗНОЙДЗЕНЫЯ СІМВАЛЫ</b>' . self::BR . self::BR;
			$html .= $this->resultTable;
			$html .= '</body></html>';
			fwrite($new_file, $html);
			fclose($new_file);
			if(mb_strlen($this->resultTable)) {
				$mail_body .= "Выніковая табліца пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,2}/u', $this->resultTable, $matches);
				$mail_body .= '<i>' . str_replace("\r\n", self::BR, trim($matches[0])) . '</i>';
			}
			$mail_body .= "Выніковы html-файл захаваны тут: " . $filepath;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <$senders_email>\r\n";
			mail($recipient, $subject, $mail_body, $header);
			
			$filename = $date_code . '_' . $ip . '_' . $rand_code . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mail_body, $header)));
			fclose($newFile);
		}
		
		public function getCharactersToNames() { return $this->charactersToNames; }
		public function getResultTable() { return $this->resultTable; }
	}
?>