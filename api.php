<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');

	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$certainCharacters = isset($_POST['certainCharacters']) ? $_POST['certainCharacters'] : '';

	include_once 'CharacterFrequencyCounter.php';
	CharacterFrequencyCounter::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$CharacterFrequencyCounter = new CharacterFrequencyCounter($localization, $certainCharacters);
		$CharacterFrequencyCounter->setText($text);
		$CharacterFrequencyCounter->run();
		$CharacterFrequencyCounter->saveCacheFiles();

		$result['text'] = $text;
		$result['result'] = $CharacterFrequencyCounter->getResultTable();
		$msg = json_encode($result);
	}
	else {
		$result['text'] = $text;
		$result['result'] = 'Вы даслалі пусты запыт!';
		$msg = json_encode($result);
	}
	echo $msg;
?>
